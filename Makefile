.PHONY: unity clean test all

all: test
	$(MAKE) -C src

test: unity
	$(MAKE) -C test

unity:
	$(MAKE) -C unity

clean:
	$(MAKE) -C unity clean
	$(MAKE) -C test clean
	$(MAKE) -C src clean