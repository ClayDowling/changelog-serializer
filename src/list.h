#ifndef _LIST_H_
#define _LIST_H_

typedef struct {
  void *TOP;
  void *tail;
} list_t;

list_t *list_create(void);

#endif