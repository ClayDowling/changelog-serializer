#include "column.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

column_t *column_create(char *name, char *datatype, int length, int precision,
                        int scale) {
  column_t *c = (column_t *)calloc(1, sizeof(column_t));
  c->name = strdup(name);
  c->datatype = strdup(datatype);
  c->length = length;
  c->precision = precision;
  c->scale = scale;

  return c;
}

void column_destroy(column_t *c) {
  column_t *next = c->next;
  if (c->name)
    free(c->name);
  if (c->datatype)
    free(c->datatype);
  c->next = NULL;
  c->name = NULL;
  c->datatype = NULL;

  free(c);
  column_destroy(next);
}

column_t *column_from_json(cJSON *json) {

  char *name = NULL;
  char *datatype = NULL;
  int length = 0;
  int precision = 0;
  int scale = 0;

  for (cJSON *cur = json->child; cur != NULL; cur = cur->next) {
    if (strcasecmp(cur->string, "name") == 0) {
      name = cur->valuestring;
    } else if (strcasecmp(cur->string, "datatype") == 0) {
      datatype = cur->valuestring;
    } else if (strcasecmp(cur->string, "length") == 0) {
      length = cur->valueint;
    } else if (strcasecmp(cur->string, "precision") == 0) {
      precision = cur->valueint;
    } else if (strcasecmp(cur->string, "scale") == 0) {
      scale = cur->valueint;
    }
  }

  return column_create(name, datatype, length, precision, scale);
}

list_t column_list_from_json(cJSON *json) {
  list_t lst;
  lst.TOP = NULL;
  lst.tail = NULL;
  column_t *c;

  for (cJSON *cur = json->child; NULL != cur; cur = cur->next) {
    c = column_from_json(cur);
    if (NULL == lst.TOP) {
      lst.TOP = c;
    }
    if (lst.tail) {
      column_t *tail = lst.tail;
      tail->next = c;
    }
    lst.tail = c;
  }

  return lst;
}

cJSON *column_to_json(column_t *c) {
  cJSON *json = cJSON_CreateObject();
  cJSON_AddStringToObject(json, "name", c->name);
  cJSON_AddStringToObject(json, "dataType", c->datatype);
  cJSON_AddNumberToObject(json, "length", c->length);
  cJSON_AddNumberToObject(json, "precision", c->precision);
  cJSON_AddNumberToObject(json, "scale", c->scale);
  return json;
}

cJSON *column_list_to_json(column_t *cc) {
  cJSON *json = cJSON_CreateArray();
  for (column_t *cur = cc; NULL != cur; cur = cur->next) {
    cJSON_AddItemToArray(json, column_to_json(cur));
  }
  return json;
}
