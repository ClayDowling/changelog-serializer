#include "changevalue.h"
#include <stdlib.h>
#include <string.h>

change_value_t *change_value_create(char *new, char *old) {
  change_value_t *cv = (change_value_t *)calloc(1, sizeof(change_value_t));
  cv->new = strdup(new);
  cv->old = strdup(old);

  return cv;
}

void change_value_destroy(change_value_t *cv) {
  if (cv->new)
    free(cv->new);
  if (cv->old)
    free(cv->old);
  cv->new = NULL;
  cv->old = NULL;
  free(cv);
}

change_value_t *change_value_from_json(cJSON *json) {
  char *old = NULL;
  char *new = NULL;

  for (cJSON *cur = json->child; cur != NULL; cur = cur->next) {
    if (strcasecmp(cur->string, "old") == 0) {
      old = cur->valuestring;
    } else if (strcasecmp(cur->string, "new") == 0) {
      new = cur->valuestring;
    }
  }

  return change_value_create(new, old);
}

cJSON *change_value_to_json(change_value_t *cv) {
  cJSON *json = cJSON_CreateObject();
  cJSON_AddStringToObject(json, "new", cv->new);
  cJSON_AddStringToObject(json, "old", cv->old);
  return json;
}
