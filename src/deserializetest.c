#include "cJSON.h"
#include "changevalue.h"
#include "column.h"
#include "list.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

struct _value_column {
  column_t *column;
  change_value_t *value;
  struct _value_column *next;
};
typedef struct _value_column value_column_t;

value_column_t *value_column_create(column_t *column, change_value_t *value);
void value_column_destroy(value_column_t *);
value_column_t *value_column_from_json(cJSON *);
list_t value_column_list_from_json(cJSON *);
cJSON *value_column_to_json(value_column_t *);
cJSON *value_column_list_to_json(value_column_t *);

typedef struct {
  char *utcDateTime;
  int transactionId;
  char *userId;
  char operation;
  char *tableName;
  char *schema;
  list_t uniqueKeys;
  list_t columns;
} changelog_t;

changelog_t *changelog_create(char *utcDateTime, int transactionId,
                              char *userId, char operation, char *tablename,
                              char *schema);
void changelog_destroy(changelog_t *);
void changelog_add_key(changelog_t *, column_t *);
void changelog_add_value(changelog_t *, value_column_t *);
changelog_t *changelog_from_json(cJSON *);
cJSON *changelog_to_json(changelog_t *);

value_column_t *value_column_create(column_t *column, change_value_t *value) {
  value_column_t *vc = (value_column_t *)calloc(1, sizeof(value_column_t));
  vc->column = column;
  vc->value = value;

  return vc;
}

void value_column_destroy(value_column_t *vc) {
  value_column_t *next = vc->next;
  column_destroy(vc->column);
  change_value_destroy(vc->value);

  vc->column = NULL;
  vc->value = NULL;
  vc->next = NULL;

  value_column_destroy(next);
}

value_column_t *value_column_from_json(cJSON *json) {
  column_t *col = column_from_json(json);
  change_value_t *vals = NULL;
  for (cJSON *cur = json->child; cur != NULL; cur = cur->next) {
    if (strcasecmp(cur->string, "value") == 0) {
      vals = change_value_from_json(cur);
    }
  }
  return value_column_create(col, vals);
}

list_t value_column_list_from_json(cJSON *json) {
  list_t lst;
  value_column_t *vc = NULL;

  for (cJSON *cur = json->child; NULL != cur; cur = cur->next) {
    vc = value_column_from_json(cur);
    if (NULL == lst.TOP) {
      lst.TOP = vc;
    }
    if (lst.tail) {
      value_column_t *tail = lst.tail;
      tail->next = vc;
    }
    lst.tail = vc;
  }

  return lst;
}

cJSON *value_column_to_json(value_column_t *vc) {
  cJSON *json = column_to_json(vc->column);
  cJSON_AddItemToObject(json, "value", change_value_to_json(vc->value));

  return json;
}

cJSON *value_column_list_to_json(value_column_t *vcl) {
  cJSON *json = cJSON_CreateArray();
  for (value_column_t *cur = vcl; NULL != cur; cur = cur->next) {
    cJSON_AddItemToArray(json, value_column_to_json(cur));
  }
  return json;
}

changelog_t *changelog_create(char *utcDateTime, int transactionId,
                              char *userId, char operation, char *tablename,
                              char *schema) {
  changelog_t *cl = (changelog_t *)calloc(1, sizeof(changelog_t));
  cl->utcDateTime = strdup(utcDateTime);
  cl->transactionId = transactionId;
  cl->userId = strdup(userId);
  cl->operation = operation;
  cl->tableName = strdup(tablename);
  cl->schema = strdup(schema);
  return cl;
}

void changelog_destroy(changelog_t *cl) {
  if (cl->utcDateTime)
    free(cl->utcDateTime);
  if (cl->userId)
    free(cl->userId);
  if (cl->tableName)
    free(cl->tableName);
  if (cl->schema)
    free(cl->schema);

  if (cl->uniqueKeys.TOP) {
    column_destroy(cl->uniqueKeys.TOP);
  }

  if (cl->columns.TOP) {
    value_column_destroy(cl->columns.TOP);
  }

  cl->utcDateTime = NULL;
  cl->userId = NULL;
  cl->tableName = NULL;
  cl->schema = NULL;
  cl->uniqueKeys.TOP = NULL;
  cl->uniqueKeys.tail = NULL;
  cl->columns.TOP = NULL;
  cl->columns.tail = NULL;
}

void changelog_add_key(changelog_t *cl, column_t *k) {
  if (cl->uniqueKeys.TOP = NULL) {
    cl->uniqueKeys.TOP = k;
  }
  if (cl->uniqueKeys.tail) {
    column_t *tail = cl->uniqueKeys.tail;
    tail->next = k;
  }
  cl->uniqueKeys.tail = k;
}

void changelog_add_value(changelog_t *cl, value_column_t *v) {
  if (cl->columns.TOP = NULL) {
    cl->columns.TOP = v;
  }
  if (NULL == cl->columns.tail) {
    value_column_t *tail = cl->columns.tail;
    tail->next = v;
  }
  cl->columns.tail = v;
}

changelog_t *changelog_from_json(cJSON *json) {
  changelog_t *cl = NULL;
  char *utcDateTime = NULL;
  int transactionId = -1;
  char *userId = NULL;
  char operation = 0;
  char *tableName = NULL;
  char *schema = NULL;
  list_t uniqueKeys;
  list_t columns;

  for (cJSON *cur = json->child; NULL != cur; cur = cur->next) {
    if (strcasecmp(cur->string, "utcDateTime") == 0) {
      utcDateTime = cur->valuestring;
    } else if (strcmp("transactionId", cur->string) == 0) {
      transactionId = cur->valueint;
    } else if (strcmp(cur->string, "userId") == 0) {
      userId = cur->valuestring;
    } else if (strcmp(cur->string, "operation") == 0) {
      operation = cur->valuestring[0];
    } else if (strcmp(cur->string, "tableName") == 0) {
      tableName = cur->valuestring;
    } else if (strcmp(cur->string, "schema") == 0) {
      schema = cur->valuestring;
    } else if (strcmp(cur->string, "uniqueKeys") == 0) {
      uniqueKeys = column_list_from_json(cur->child);
    } else if (strcmp(cur->string, "columns") == 0) {
      columns = value_column_list_from_json(cur->child);
    }
  }

  cl = changelog_create(utcDateTime, transactionId, userId, operation,
                        tableName, schema);
  cl->uniqueKeys = uniqueKeys;
  cl->columns = columns;

  return cl;
}

cJSON *changelog_to_json(changelog_t *cl) {
  char operation[2] = {cl->operation, 0};
  cJSON *json = cJSON_CreateObject();
  cJSON_AddStringToObject(json, "utcDateTime", cl->utcDateTime);
  cJSON_AddNumberToObject(json, "transactionId", cl->transactionId);
  cJSON_AddStringToObject(json, "userId", cl->userId);
  cJSON_AddStringToObject(json, "operation", operation);
  cJSON_AddStringToObject(json, "tableName", cl->tableName);
  cJSON_AddStringToObject(json, "schema", cl->schema);
  cJSON_AddItemToObject(json, "uniqueKeys",
                        column_list_to_json(cl->uniqueKeys.TOP));
  cJSON_AddItemToObject(json, "columns",
                        value_column_list_to_json(cl->columns.TOP));
  return json;
}

int main(int argc, char **argv) {
  FILE *in;
  char *src;
  struct stat sb;

  if (stat("changelog.json", &sb)) {
    perror("changelog.json");
    return EXIT_FAILURE;
  }
  in = fopen("changelog.json", "r");
  src = calloc(1, sb.st_size + 1);
  fread(src, sb.st_size, 1, in);
  fclose(in);

  cJSON *json = cJSON_Parse(src);

  changelog_t *cl = changelog_from_json(json);
  cJSON *generated = changelog_to_json(cl);

  if (cJSON_Compare(json, generated, cJSON_False) == cJSON_False) {
    char *src = cJSON_Print(json);
    char *gen = cJSON_Print(generated);

    printf("Source:\n%s\nGenerated:\n%s\n", src, gen);
    free(src);
    free(gen);
  } else {
    printf("Success\n");
  }

  free(src);
  return EXIT_SUCCESS;
}