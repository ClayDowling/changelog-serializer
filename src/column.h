#ifndef _COLUMN_H_
#define _COLUMN_H_

#include "cJSON.h"
#include "list.h"

struct _column {
  char *name;
  char *datatype;
  int length;
  int precision;
  int scale;
  struct _column *next;
};
typedef struct _column column_t;

column_t *column_create(char *name, char *datatype, int length, int precision,
                        int scale);
void column_destroy(column_t *);
column_t *column_from_json(cJSON *);
list_t column_list_from_json(cJSON *);
cJSON *column_to_json(column_t *);
cJSON *column_list_to_json(column_t *);

#endif