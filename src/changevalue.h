#ifndef _CHANGEVALUE_H_
#define _CHANGEVALUE_H_

#include "cJSON.h"

typedef struct {
  char *new;
  char *old;
} change_value_t;

change_value_t *change_value_create(char *new, char *old);
void change_value_destroy(change_value_t *);
change_value_t *change_value_from_json(cJSON *);
cJSON *change_value_to_json(change_value_t *);

#endif