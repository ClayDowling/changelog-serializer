#include "column.h"
#include "unity_fixture.h"
#include <string.h>

#define NAME "COLUMN_NAME"
#define DATATYPE "varchar"
#define LENGTH 25
#define PRECISION 1
#define SCALE 2

TEST_GROUP(Column);

TEST_SETUP(Column) {}

TEST_TEAR_DOWN(Column) {}

TEST(Column, columnCreate_ByDefault_InitializesMembers) {
  column_t *actual = column_create(NAME, DATATYPE, LENGTH, PRECISION, SCALE);
  TEST_ASSERT_EQUAL_STRING(NAME, actual->name);
  TEST_ASSERT_EQUAL_STRING(DATATYPE, actual->datatype);
  TEST_ASSERT_EQUAL_INT(LENGTH, actual->length);
  TEST_ASSERT_EQUAL_INT(PRECISION, actual->precision);
  TEST_ASSERT_EQUAL_INT(SCALE, actual->scale);
  TEST_ASSERT_NULL(actual->next);
}

TEST(Column, columnFromJson_byDefault_PopulatesAllMembers) {
  cJSON *json = cJSON_CreateObject();
  cJSON_AddStringToObject(json, "name", NAME);
  cJSON_AddStringToObject(json, "datatype", DATATYPE);
  cJSON_AddNumberToObject(json, "length", LENGTH);
  cJSON_AddNumberToObject(json, "precision", PRECISION);
  cJSON_AddNumberToObject(json, "scale", SCALE);

  column_t *actual = column_from_json(json);

  TEST_ASSERT_EQUAL_STRING(NAME, actual->name);
  TEST_ASSERT_EQUAL_STRING(DATATYPE, actual->datatype);
  TEST_ASSERT_EQUAL_INT(LENGTH, actual->length);
  TEST_ASSERT_EQUAL_INT(PRECISION, actual->precision);
  TEST_ASSERT_EQUAL_INT(SCALE, actual->scale);
  TEST_ASSERT_NULL(actual->next);
}

TEST(Column, columnListFromJson_byDefault_PopulatesAllMembers) {
  cJSON *json1 = cJSON_CreateObject();
  cJSON_AddStringToObject(json1, "name", NAME);
  cJSON_AddStringToObject(json1, "datatype", DATATYPE);
  cJSON_AddNumberToObject(json1, "length", LENGTH);
  cJSON_AddNumberToObject(json1, "precision", PRECISION);
  cJSON_AddNumberToObject(json1, "scale", SCALE);

  cJSON *json2 = cJSON_CreateObject();
  cJSON_AddStringToObject(json2, "name", NAME "2");
  cJSON_AddStringToObject(json2, "datatype", DATATYPE "2");
  cJSON_AddNumberToObject(json2, "length", LENGTH);
  cJSON_AddNumberToObject(json2, "precision", PRECISION);
  cJSON_AddNumberToObject(json2, "scale", SCALE);

  cJSON *json = cJSON_CreateArray();
  cJSON_AddItemToArray(json, json1);
  cJSON_AddItemToArray(json, json2);

  list_t actual = column_list_from_json(json);

  column_t *first = actual.TOP;

  TEST_ASSERT_EQUAL_STRING(NAME, first->name);
  TEST_ASSERT_EQUAL_STRING(DATATYPE, first->datatype);
  TEST_ASSERT_EQUAL_INT(LENGTH, first->length);
  TEST_ASSERT_EQUAL_INT(PRECISION, first->precision);
  TEST_ASSERT_EQUAL_INT(SCALE, first->scale);
  TEST_ASSERT_NOT_NULL(first->next);

  column_t *second = first->next;
  TEST_ASSERT_EQUAL_STRING(NAME "2", second->name);
  TEST_ASSERT_EQUAL_STRING(DATATYPE "2", second->datatype);
  TEST_ASSERT_EQUAL_INT(LENGTH, second->length);
  TEST_ASSERT_EQUAL_INT(PRECISION, second->precision);
  TEST_ASSERT_EQUAL_INT(SCALE, second->scale);
  TEST_ASSERT_NULL(second->next);
}

TEST(Column, ColumnToJson_byDefault_SerializesAllFields) {
  column_t *origin = column_create(NAME, DATATYPE, LENGTH, PRECISION, SCALE);
  cJSON *json = column_to_json(origin);

  char *name = NULL;
  char *datatype = NULL;
  int length = 0;
  int precision = 0;
  int scale = 0;

  for (cJSON *cur = json->child; cur; cur = cur->next) {
    if (strcasecmp("name", cur->string) == 0) {
      name = cur->valuestring;
      continue;
    }
    if (strcasecmp("dataType", cur->string) == 0) {
      datatype = cur->valuestring;
      continue;
    }
    if (strcasecmp("length", cur->string) == 0) {
      length = cur->valueint;
      continue;
    }
    if (strcasecmp("precision", cur->string) == 0) {
      precision = cur->valueint;
      continue;
    }
    if (strcasecmp("scale", cur->string) == 0) {
      scale = cur->valueint;
      continue;
    }
  }

  TEST_ASSERT_EQUAL_STRING(NAME, name);
  TEST_ASSERT_EQUAL_STRING(DATATYPE, datatype);
  TEST_ASSERT_EQUAL_INT(LENGTH, length);
  TEST_ASSERT_EQUAL_INT(PRECISION, precision);
  TEST_ASSERT_EQUAL_INT(SCALE, scale);
}

TEST(Column, ColumnListToJson_byDefault_CreatesJsonArray) {
  list_t lst;

  column_t *first = column_create(NAME, DATATYPE, LENGTH, PRECISION, SCALE);
  column_t *second =
      column_create(NAME "2", DATATYPE "2", LENGTH, PRECISION, SCALE);
  first->next = second;

  cJSON *json = column_list_to_json(first);
  cJSON *cur = NULL;
  int count = 0;
  TEST_ASSERT_TRUE(cJSON_IsArray(json));
  cJSON_ArrayForEach(cur, json) { ++count; }
  TEST_ASSERT_EQUAL_INT(2, count);
}

TEST_GROUP_RUNNER(Column) {
  RUN_TEST_CASE(Column, columnCreate_ByDefault_InitializesMembers);
  RUN_TEST_CASE(Column, columnFromJson_byDefault_PopulatesAllMembers);
  RUN_TEST_CASE(Column, columnListFromJson_byDefault_PopulatesAllMembers);
  RUN_TEST_CASE(Column, ColumnToJson_byDefault_SerializesAllFields);
  RUN_TEST_CASE(Column, ColumnListToJson_byDefault_CreatesJsonArray);
}