#include "changevalue.h"
#include "unity_fixture.h"
#include <string.h>

#define NEW_VALUE "I'm new"
#define OLD_VALUE "I'm busted"

TEST_GROUP(ChangeValue);

TEST_SETUP(ChangeValue) {}

TEST_TEAR_DOWN(ChangeValue) {}

TEST(ChangeValue, changeValueCreate_byDefault_InitializesMembers) {
  change_value_t *actual = change_value_create(NEW_VALUE, OLD_VALUE);
  TEST_ASSERT_EQUAL_STRING(NEW_VALUE, actual->new);
  TEST_ASSERT_EQUAL_STRING(OLD_VALUE, actual->old);
}

TEST_GROUP_RUNNER(ChangeValue) {
  RUN_TEST_CASE(ChangeValue, changeValueCreate_byDefault_InitializesMembers);
}